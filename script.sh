#!/bin/bash

cd /Users/simon/projects/covid
URL=$(
  curl -Ls https://www.covid19.act.gov.au/act-status-and-response/act-covid-19-exposure-locations | \
    grep -F Papa.parse | \
    cut -f 2 -d '"'
)
curl -s "$URL" | /usr/local/bin/php script.php > locations.csv
git diffcsv -w
git add locations.csv
