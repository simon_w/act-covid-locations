<?php

$rows = [];

while ($row = \fgetcsv(STDIN)) {
  $rows[] = \array_map('trim', \array_slice($row, 1,  9));
}

usort($rows, function($a, $b) {
  return strcasecmp($a[1].$a[5].$a[6].$a[7], $b[1].$b[5].$b[6].$b[7]);
});

foreach($rows as $row) fputcsv(STDOUT, $row);
